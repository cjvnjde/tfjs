import p5 from 'p5';

import Field from './field';
import Model from './model';

let field;
let model;
const resolution = 400 / 20;
const xs = [
  [0, 1],
  [1, 1],
  [0, 0],
  [1, 0]
];
const ys = [
  [0],
  [1],
  [1],
  [0],
]

let testData = [];

for (let i = 0; i < 1; i += 1 / 20) {
  for (let j = 0; j < 1; j += 1 / 20) {
    testData.push([i, j])
  }
}

const sketch = (sk) => {
  sk.setup = () => {
    sk.createCanvas(
      400,
      400,
    );

    field = new Field(sk, resolution);
    model = new Model();
    model.setTrainData(xs, ys);
    model.setPredictionData(testData);
    model.trainModel();
  }

  sk.draw = () => {
    sk.background(0);
    field.update(model.prediction());
  }
}

new p5(sketch);
