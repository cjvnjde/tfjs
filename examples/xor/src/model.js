import * as tf from '@tensorflow/tfjs';

class Model {
  constructor() {
    this.model;
    this.xs;
    this.ys;
    window.tf = tf;
    this.createModel();
  }

  createModel = () => {
    this.model = tf.sequential();

    const hidden = tf.layers.dense({
      inputShape: [2],
      units: 2,
      useBias: true,
      activation: 'selu',
    });

    const output = tf.layers.dense({
      units: 1,
      useBias: true,
      activation: 'sigmoid',
    })

    this.model.add(hidden);
    this.model.add(output);

    const optimizer = tf.train.adam(0.2);

    this.model.compile({
      optimizer,
      loss: 'meanSquaredError',
    })
  }

  setTrainData = (xs, ys) => {
    this.xs = tf.tensor2d(xs);
    this.ys = tf.tensor2d(ys);
  }

  trainModel = () => {
    this.model.fit(this.xs, this.ys, {
      shuffle: true,
      epochs: 10,
    }).then(() => {
      setTimeout(this.trainModel, 50);
    });
  }

  setPredictionData = (xs) => {
    this.xs_p = tf.tensor2d(xs)
  }

  prediction = () => {
    return tf.tidy(()=> this.model.predict(this.xs_p).dataSync());
  }
}

export default Model;