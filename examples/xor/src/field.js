class Field {
  constructor(sk, resolution) {
    for(let f in sk) {
      this[f] = sk[f];
    }

    this.resolution = resolution;
  }

  update = (fillData) => {
    let index = 0;
    for(let i = 0; i < this.width / this.resolution; i++) {
      for(let j = 0; j < this.height / this.resolution; j++) {
        this.fill(fillData[index]*255);
        // console.log(fillData[index]);
        this.rect(
          j * this.resolution,
          i * this.resolution,
          this.resolution,
          this.resolution);

          index++;
      }
    }
  }
}

export default Field;