import Matter from 'matter-js';

const Engine = Matter.Engine,
      Render = Matter.Render,
      Runner = Matter.Runner,
      Body = Matter.Body,
      Composites = Matter.Composites,
      Constraint = Matter.Constraint,
      MouseConstraint = Matter.MouseConstraint,
      Mouse = Matter.Mouse,
      World = Matter.World,
      Events = Matter.Events,
      Bodies = Matter.Bodies;

const engine = Engine.create();

const render = Render.create({
  element: document.body,
  engine,
  options: {
    width: 800,
    height: 600,
    showAngleIndicator: true,
  },
});

Render.run(render);

const runner = Runner.create();
const world = engine.world;

Runner.run(runner, engine);

const circleA = Bodies.rectangle(210, 230, 120, 20, {
  mass: 1,
});
const rectangleBottom = Bodies.rectangle(200, 200, 60, 20, {
  friction: 0,
  mass: 10,
});

const constraint = Constraint.create({
  bodyA: circleA,
  pointA: { x: 60, y: 0 },
  bodyB: rectangleBottom,
  pointB: { x: 0, y: 0 },
  length: 0,
});

World.add(world, [
  circleA,
  constraint,
  rectangleBottom,
  Bodies.rectangle(400, 0, 800, 50, { isStatic: true }),
  Bodies.rectangle(800, 300, 50, 600, { isStatic: true }),
  Bodies.rectangle(0, 300, 50, 600, { isStatic: true }),
  Bodies.rectangle(400, 605, 800, 50, { isStatic: true })
]);

Render.lookAt(render, {
  min: { x: 0, y: 0 },
  max: { x: 800, y: 600 }
});

// add mouse control
var mouse = Mouse.create(render.canvas),
mouseConstraint = MouseConstraint.create(engine, {
    mouse: mouse,
    constraint: {
        stiffness: 0.2,
        render: {
            visible: false
        }
    }
});

World.add(world, mouseConstraint);

// keep the mouse in sync with rendering
render.mouse = mouse;

Events.on(engine, "beforeUpdate", () => {
  Body.setPosition(rectangleBottom, {x: rectangleBottom.position.x, y: 200});
})
Events.on(engine, "afterUpdate", () => {
  Body.setPosition(rectangleBottom, {x: rectangleBottom.position.x, y: 200});
})
Events.on(engine, "collisionActive", () => {
  console.log('end');
  console.log(circleA)
})

const KEY_A = 65;
const KEY_D = 68;

window.addEventListener('keydown', (e) => {
  const code = e.keyCode;
  if (code == KEY_A) {
    Body.setVelocity(rectangleBottom, {x: -5, y: rectangleBottom.velocity.y});
    Body.setAngularVelocity(rectangleBottom, {x: 0, y: 0});
  };
  if (code == KEY_D) {
    Body.setVelocity(rectangleBottom, {x: 5, y: rectangleBottom.velocity.y});
    Body.setAngularVelocity(rectangleBottom, {x: 0, y: 0});
  };
}, false);
