class Field {
  constructor(sk) {
    for(let f in sk) {
      this[f] = sk[f];
    }
  }

  update = () => {
    this.fill(0);
    this.rect(
      0,
      0,
      this.width,
      20,
    );
    this.rect(
      0,
      0,
      20,
      this.height,
    );
    this.rect(
      this.width-20,
      0,
      20,
      this.height,
    );
  }
}

export default Field;