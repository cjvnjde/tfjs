import p5 from 'p5';

import Field from './field';

let field;
const sketch = (sk) => {
  sk.setup = () => {
    sk.createCanvas(400, 400);
    field = new Field(sk);
    console.log(field)
    field.update();
  };

  sk.draw = () => {

  };
}

new p5(sketch);