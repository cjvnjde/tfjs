import p5 from 'p5';
import * as tf from '@tensorflow/tfjs';

const xVals = [];
const yVals = [];

let a;
let b;
let c;
let d;

const learningRate = 0.1;
const optimizer = tf.train.adam(learningRate);

const sketch = (sk) => {
  sk.setup = () => {
    sk.createCanvas(400, 400);

    a = tf.variable(tf.scalar(sk.random(-1, 1)));
    b = tf.variable(tf.scalar(sk.random(-1, 1)));
    c = tf.variable(tf.scalar(sk.random(-1, 1)));
    d = tf.variable(tf.scalar(sk.random(-1, 1)));
  };

  function predict(x) {
    const xs = tf.tensor1d(x);
    // y = a*x^3 + b*x^2 + c*x + c
    const ys = xs.pow(3).mul(a)
      .add(xs.square().mul(b))
      .add(xs.mul(c))
      .add(d);
    return ys;
  }

  function loss(pred, labels) {
    return pred.sub(labels).square().mean();
  }

  sk.draw = () => {
    tf.tidy(() => {
      if (xVals.length > 0) {
        const ys = tf.tensor1d(yVals);
        optimizer.minimize(() => loss(predict(xVals), ys));
      }
    });

    sk.background(0);

    sk.stroke(255);
    sk.strokeWeight(8);

    for (let i = 0; i < xVals.length; i += 1) {
      const px = sk.map(xVals[i], -1, 1, 0, sk.width);
      const py = sk.map(yVals[i], -1, 1, sk.height, 0);

      sk.point(px, py);
    }

    const curveX = [];
    for (let x = -1; x < 1.01; x += 0.05) {
      curveX.push(x);
    }

    const ys = tf.tidy(() => predict(curveX));
    const curveY = ys.dataSync();
    ys.dispose();

    sk.beginShape();
    sk.noFill();
    sk.stroke(255);
    sk.strokeWeight(2);

    for (let i = 0; i < curveX.length; i += 1) {
      const x = sk.map(curveX[i], -1, 1, 0, sk.width);
      const y = sk.map(curveY[i], -1, 1, sk.height, 0);
      sk.vertex(x, y);
    }
    sk.endShape();
  };

  sk.mousePressed = () => {
    const x = sk.map(sk.mouseX, 0, sk.width, -1, 1);
    const y = sk.map(sk.mouseY, 0, sk.height, 1, -1);

    xVals.push(x);
    yVals.push(y);
  };
};

// eslint-disable-next-line no-new
new p5(sketch);
